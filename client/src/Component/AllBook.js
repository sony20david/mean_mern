import react, { useState, useEffect } from 'react';
import { Table, TableHead, TableCell, Paper, TableRow, TableBody, Button, makeStyles } from '@material-ui/core'
import { getBooks, deleteBook} from '../Service/api';
import { Link } from 'react-router-dom';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';



const useStyles = makeStyles({
    table: {
        width: '80%',
        margin: '30px 0 0 30px',
        backgroundColor:"#F5E6ED",
        
    },
    container:{
        overflowX:"auto",
    },
    thead: {
        '& > *': {
            fontSize: 15,
            background: '#B1A3C7',
            color: '#FFFFFF'
        }
    },
    row: {
        '& > *': {
            fontSize: 14
        }
    },
    green:{
        color:"green"
    },
    red:{
        color:"red"
    },
    
    
})





const AllBook = () => {
    const [books, setBooks] = useState([]);
    const classes = useStyles();


    useEffect(() => {
        getAllBooks();
    }, []);

    const deleteBookData = async (id) => {
        await deleteBook(id);
        getAllBooks();
    }

    const getAllBooks = async () => {
        let response = await getBooks();
        setBooks(response.data);
    }

   

    return (
        <div className={classes.container}>
        <Table className={classes.table}>
            <TableHead>
                <TableRow className={classes.thead}>
                    <TableCell>Book Name</TableCell>
                    <TableCell >Book Description</TableCell>
                    <TableCell>Book Price</TableCell>
                    <TableCell>Book Discount</TableCell>
                    <TableCell>Book Availability</TableCell>
                    <TableCell>Book Author</TableCell>
                    <TableCell>Book Publisher</TableCell>
                    <TableCell>Edit</TableCell>
                    <TableCell>Delete</TableCell>
                    
                </TableRow>
            </TableHead>
            <TableBody>
                {books.map((book) => (
                    <TableRow className={classes.row} key={book.id}>
                        {/* change it to book.id to use JSON Server */} 
                        <TableCell>{book.bookname}</TableCell>
                        <TableCell >{book.book_description}</TableCell>
                        <TableCell>{book.book_price}</TableCell>
                        <TableCell style={{color:"red"}}>{book.book_discount+"%"}</TableCell>
                        <TableCell className={book.book_availability}>{book.book_availability} </TableCell>
                        <TableCell>{book.book_author}</TableCell>
                        <TableCell>{book.book_publisher}</TableCell>
                        <TableCell>
                            <Button   style={{marginRight:10,backgroundColor:"#CEBCE0"}} component={Link} to={`/edit/${book._id}`}><EditIcon/></Button> {/* change it to book.id to use JSON Server */}
                            </TableCell>
                        
                            <TableCell>
                            <Button style={{backgroundColor:"#D0A1C2"}} onClick={() => deleteBookData(book._id)}><DeleteIcon /></Button> {/* change it to book.id to use JSON Server */}
                            </TableCell>
                            
                    </TableRow>
                ))}
            </TableBody>
        </Table>
        </div>
    )
}

export default AllBook;