import react, { useState } from "react";
import {
  FormGroup,
  FormHelperText,
  FormControl,
  InputLabel,
  Input,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { addBook } from "../Service/api";
import { useHistory } from "react-router-dom";
import M from "materialize-css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import {validate_book_name, validate_discount,validate_author_name, validate_description, validate_price, validate_stock, validate_publisher_name} from './validation'

toast.configure();

const initialValue = {
  bookname: "",
  book_description: "",
  book_price: "",
  book_discount: "",
  book_availability: "",
  book_author: "",
  book_publisher: "",
  book_type: "",
  booknameError: "",
};

const useStyles = makeStyles({
  container: {
    width: "50%",
    margin: "5% 0 0 15%",
    "& > *": {
      marginTop: 20,
    },
    backgroundColor: "#F8ECF2",
  },
  bg: {
    display: "flex",
    flexDirection: "row",
    flexwrap: "wrap",
    backgroundColor: "#F8ECF2",
    overFit: "auto",
  },
  error: {
    color: "red",
    fontSize: "13px",
  },
});

const Try = () => {
  const [book, setBook] = useState(initialValue);
  let [errorMessage, setErrorMessage] = useState("");
  let [desErrorMessage,setdesErrorMessage]=useState("");
  const {
    bookname,
    book_description,
    book_price,
    book_discount,
    book_availability,
    book_author,
    book_publisher,
    book_type,
   } = book;
  const classes = useStyles();
  const history = useHistory();

  const addBookDetails = async () => {

    let ErrorMessage = validate_book_name(bookname)
    errorMessage=ErrorMessage
    if (errorMessage !== "Success") {
        setErrorMessage(errorMessage)
    }


    let deserrorMessage=validate_description(book_description)
    desErrorMessage=deserrorMessage;
    if(desErrorMessage!=="Success"){
        setdesErrorMessage(desErrorMessage)
    }
    
    else{
    await addBook(book);
    toast.success("Book details added successfully", {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 1000,
    });
    history.push("./");
}
  };
  const addbookfield = (e) => {
   
    setBook({ ...book, bookname: e.target.value });
  };
  const adddesfield = (e) => {
    
    setBook({ ...book, book_description: e.target.value });
  };
  const addpricefield = (e) => {
    
    setBook({ ...book, book_price: e.target.value });
  };
  const adddiscountfield = (e) => {
    
    setBook({ ...book, book_discount: e.target.value });
  };
  const addavailfield = (e) => {
    
    setBook({ ...book, book_availability: e.target.value });
  };
  const addauthorfield = (e) => {
    
    setBook({ ...book, book_author: e.target.value });
  };
  const addpublisherfield = (e) => {
    
    setBook({ ...book, book_publisher: e.target.value });
  };
  const addtypefield = (e) => {
   
    setBook({ ...book, book_type: e.target.value });
  };

  return (
    <div className={classes.bg}>
      {/* <MediaCard/> */}

      <FormGroup className={classes.container}>
        <Typography variant="h4">Add Book</Typography>
        <FormControl xs={12} md={6}>
          <InputLabel htmlFor="my-input">Name</InputLabel>
          <Input
            onChange={addbookfield}
            name="bookname"
            value={book.bookname}
            id="my-input"
          />
          {errorMessage && (
            <span className={classes.error}> {errorMessage} </span>
          )}
       
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">Description</InputLabel>
          <Input
            onChange={adddesfield}
            name="description"
            value={book.book_description}
            id="my-input"
          />
          {desErrorMessage && (
            <span className={classes.error}> {desErrorMessage} </span>
          )}
          
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">price</InputLabel>
          <Input
            onChange={addpricefield}
            name="price"
            value={book.book_price}
            id="my-input"
          />
          
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">discount</InputLabel>
          <Input
            onChange={adddiscountfield}
            name="discount"
            value={book.book_discount}
            id="my-input"
          />
        
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">availability</InputLabel>
          <Input
            onChange={addavailfield}
            name="availability"
            value={book.book_availability}
            id="my-input"
          />
          
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">author</InputLabel>
          <Input
            onChange={addauthorfield}
            name="author"
            value={book.book_author}
            id="my-input"
          />
          
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">publisher</InputLabel>
          <Input
            onChange={addpublisherfield}
            name="publisher"
            value={book.book_publisher}
            id="my-input"
          />
         
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="my-input">type</InputLabel>
          <Input
            required={true}
            onChange={addtypefield}
            name="publisher"
            value={book.book_type}
            id="my-input"
          />
          
        </FormControl>
        <FormControl>
          <Button
            variant="contained"
            color="primary"
            onClick={() => addBookDetails()}
          >
            Add Book
          </Button>
        </FormControl>
        <br></br>
      </FormGroup>
    </div>
  );
};

export default Try;
