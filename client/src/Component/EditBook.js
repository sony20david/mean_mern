import { useState, useEffect } from 'react';
import { FormGroup, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { getBooks, editBook } from '../Service/api';
import {toast} from 'react-toastify';

import { validate_author_name, validate_book_name,validate_description, validate_discount, validate_price, validate_publisher_name, validate_stock, validate_type_name } from './validation';
const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    },
    error:{
        color:"red",
        fontSize:"13px"
    }
})

const EditBook = () => {
    const [book, setBook] = useState(initialValue);
    let [errorMessage, setErrorMessage] = useState("");
    let [desErrorMessage, setdesErrorMessage] = useState("");
    let [priceerrorMessage, setpriceErrorMessage] = useState("");
    let [discounterrorMessage, setdiscountErrorMessage] = useState("");
    let [availabilityerrorMessage, setavailabilityErrorMessage] = useState("");
    let [authorerrorMessage, setauthorErrorMessage] = useState("");
    let [publishererrorMessage, setpublisherErrorMessage] = useState("");
    const { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher,book_type } = book;
    const { id } = useParams();
    const classes = useStyles();
    let history = useHistory();

    useEffect(() => {
        loadBookDetails();
    }, []);

    const loadBookDetails = async() => {
        const response = await getBooks(id);
        
        setBook(response.data);
    }

    const editBookDetails = async() => {

      let ErrorMessage = validate_book_name(bookname)
      errorMessage=ErrorMessage
      if (errorMessage !== "Success") {
          setErrorMessage(errorMessage)
      }
  
  
      let deserrorMessage=validate_description(book_description)
      desErrorMessage=deserrorMessage;
      if(desErrorMessage!=="Success"){
          setdesErrorMessage(desErrorMessage)
      }

      let priceErrorMessage=validate_price(book_price)
      priceerrorMessage=priceErrorMessage;
      if(priceerrorMessage!=="Success"){
        setpriceErrorMessage(priceerrorMessage)
      }

      let discountErrorMessage=validate_discount(book_discount)
      discounterrorMessage=discountErrorMessage;
      if(discounterrorMessage!=="Success"){
        setdiscountErrorMessage(discounterrorMessage)
      }

      let avail =validate_stock(book_availability)
      availabilityerrorMessage=avail;
      if(availabilityerrorMessage!=="Success"){
        setavailabilityErrorMessage(availabilityerrorMessage)
      }

      let authorerror=validate_author_name(book_author)
      authorerrorMessage=authorerror;
      if(authorerrorMessage!=="Success"){
        setauthorErrorMessage(authorerrorMessage)
      }

      let pulishererror=validate_publisher_name(book_publisher)
      publishererrorMessage=pulishererror;
      if(publishererrorMessage!=="Success"){
        setpublisherErrorMessage(publishererrorMessage)
      }

    //   let typeerror=validate_type_name(book_type)
    //   typeerrorMessage=typeerror;
    //   if(typeerrorMessage!=="Success"){
    //     settypeErrorMessage(typeerrorMessage)
    //   }
    if(errorMessage==="Success" && deserrorMessage==="Success" && priceerrorMessage==="Success"
      && discounterrorMessage==="Success" && availabilityerrorMessage==="Success" &&
      authorerrorMessage==="Success" && publishererrorMessage==="Success"){     
        const response =await editBook(id, book);
        console.log(response)
        history.push('/all');
        }
      else{
        toast.error('details are not valid', {
            position: toast.POSITION.TOP_RIGHT,autoClose:500});
      }
      
    }

    const addbookfield=(e) =>{
     setBook({...book,bookname:e.target.value})
    }
    const adddesfield=(e)=>{
        
        setdesErrorMessage("")
        setBook({...book,book_description:e.target.value})   
    }
    const addpricefield=(e)=>{
        
        setpriceErrorMessage("")
        setBook({...book,book_price:e.target.value}) 
    }
    const adddiscountfield=(e)=>{
       
        setdiscountErrorMessage("")
        setBook({...book,book_discount:e.target.value})     
    }
    const addavailfield=(e)=>{
    
        setavailabilityErrorMessage("")
        setBook({...book,book_availability:e.target.value}) 
    }

    const addauthorfield=(e)=>{
        
        setauthorErrorMessage("")
        setBook({...book,book_author:e.target.value}) 
    }
    const addpublisherfield=(e)=>{
       
        setpublisherErrorMessage("")
        setBook({...book,book_publisher:e.target.value}) 
    }

    return (
        <FormGroup className={classes.container}>
            <Typography variant="h4">Edit Bookdetails</Typography>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Name</InputLabel>
                <Input onChange={addbookfield} name='name' value={bookname} id="my-input" aria-describedby="my-helper-text" />
                {errorMessage && <span className={classes.error}> {errorMessage} </span>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Description</InputLabel>
                <Input onChange={adddesfield} name='username' value={book_description} id="my-input" aria-describedby="my-helper-text" />
                {desErrorMessage && <span className={classes.error}> {desErrorMessage} </span>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Price</InputLabel>
                <Input onChange={addpricefield} name='email' value={book_price} id="my-input" aria-describedby="my-helper-text" />
                {priceerrorMessage && <p className={classes.error}> {priceerrorMessage} </p>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Discount</InputLabel>
                <Input onChange={adddiscountfield} name='phone' value={book_discount} id="my-input" aria-describedby="my-helper-text" />
                {discounterrorMessage && <p className={classes.error}> {discounterrorMessage} </p>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Availability</InputLabel>
                <Input onChange={addavailfield} name='phone' value={book_availability} id="my-input" aria-describedby="my-helper-text" />
                {availabilityerrorMessage && <p className={classes.error}> {availabilityerrorMessage} </p>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Author</InputLabel>
                <Input onChange={addauthorfield} name='phone' value={book_author} id="my-input" aria-describedby="my-helper-text" />
                {authorerrorMessage && <p className={classes.error}> {authorerrorMessage} </p>}
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Book Publisher</InputLabel>
                <Input onChange={addpublisherfield} name='phone' value={book_publisher} id="my-input" aria-describedby="my-helper-text" />
                {publishererrorMessage && <p className={classes.error}> {publishererrorMessage} </p>}
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => editBookDetails()}>Edit Book</Button>
            </FormControl>
        
        <br></br>
        </FormGroup>
    )
}

export default EditBook;