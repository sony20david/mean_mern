/**
 * This function validates the book name
 * @param {*} book_name 
 * @returns 
 */
 const validate_book_name = (bookname) => {
    if (bookname.length === 0) {
        return "*Book name should not be empty"
    }
    else if (bookname.length <5 || bookname.length > 50) {
        return "*Book name must have min 5 chars and max 50"
    }
    else {
        return "Success"
    }
}

/**
 * 
 * @param {*} author_name 
 * @returns 
 */
const validate_author_name = (book_author) => {
    if (book_author.length === 0) {
        return "*Author name is a mandatory parameter"
    }
        else if (book_author.length <5 || book_author.length >20) {
        return "*Author name should have minimum 5 chars and maximum 20"
    }
    else if (book_author.includes("  ")) {
        return "*Author name shouldn't consist of consecutive spaces"
    }
    else if  (!/^[a-zA-Z' ]+$/.test(book_author)) {
        return "Author name should be in valid format"
    }
    else {
        return "Success"
    }

}

const validate_type_name= (book_type) => {
    if (book_type.length === 0) {
        return "*Author name is a mandatory parameter"
    }
        else if (book_type.length <5 || book_type.length >20) {
        return "*Author name should have minimum 5 chars and maximum 20"
    }
    else if (book_type.includes("  ")) {
        return "*Author name shouldn't consist of consecutive spaces"
    }
    else if  (!/^[a-zA-Z' ]+$/.test(book_type)) {
        return "Author name should be in valid format"
    }
    else {
        return "Success"
    }

}


const validate_discount=(book_discount)=>{
    if (book_discount.length === 0) {
        return "*discount is a mandatory parameter"
    }
    else if(parseInt(book_discount)<0){
        return "*discount should not be less than 0"
    }
    else if(parseInt(book_discount)>99){
        return "*discount should not exceed 99"
    }
    else {
        return "Success"
    }
}
/**
 * 
 * @param {*} description 
 * @returns 
 */
const validate_description = (book_desciption) => {
    if (book_desciption.length === 0) {
        return "*Description is a mandatory parameter"
    }
    else if (book_desciption.length > 50 ) {
        return "*Description should be of length minimum 50 chars"
    }
    else if (book_desciption.includes("  ")) {
        return "*Description shouldn't consist of consecutive spaces"
    }
    else {
        return "Success"
    }
}

/**
 * 
 * @param {*} book_price 
 * @returns 
 */
const validate_price = (book_price) => {
    console.log(book_price)
    console.log(typeof book_price)

    if (book_price.length === 0) {
        return "*Price should not be empty"
    }
    else if (isNaN(book_price)) {
        return "*Price should be a Number"
    }
    else if(parseInt(book_price)<1) {
        // const price_num = parseInt(book_price)
        // if (price_num<0) {
        //     return "*Price should not be less than zero."
        // }
        // else {
        //     return "Success"
        // }
        return "*price should not be less than zero"
    }
    else{
        return "Success"
    }
}

/**
 * 
 * @param {*} book_availability 
 * @returns 
 */
const validate_stock = (book_availability) => {
    if (book_availability.length === 0) {
        return "*Stock should not be empty"
    }
    else if(isNaN(book_availability)) {
        const stock_num = parseFloat(book_availability)
        if (!Number.isInteger(stock_num)) {
            return "*Stock should be an Integer"
        }
     }
    else {
        const stock_num = parseFloat(book_availability)
        if (!Number.isInteger(stock_num) || stock_num<0) {
            return "*Stock should be an integer and must be greater than or equal to zero"
        }
        else {
            return "Success"
        }
}
}

/**
 * 
 * @param {*} publications 
 * @returns 
 */
const validate_publisher_name = (book_publisher) => {
    if (book_publisher.length === 0) {
        return "*Name of publication is a mandatory parameter"
    }
    else if (book_publisher.length < 8 || book_publisher.length >20) {
        return "*Name of publication should have min 8 chars and max 30"
    }
    else if (book_publisher.includes("  ")) {
        return "*Publication name shouldn't consist of consecutive spaces"
    }
    else if  (!/^[a-zA-Z0-9'&._ ]+$/.test(book_publisher)) {
        return "Publication name should be in valid format"
    }
    else {
        return "Success"
    }
}


export  {validate_book_name,validate_description,validate_price,validate_type_name,validate_discount,validate_stock,validate_author_name,validate_publisher_name}