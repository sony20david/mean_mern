deserrorMessage=validate_description(book_description)
    if(deserrorMessage!=="Success"){
        setdesErrorMessage({deserrorMessage:deserrorMessage})
    }
    priceerrorMessage=validate_price(book_price)
    if(priceerrorMessage!=="Success"){
        setpriceErrorMessage({priceerrorMessage:priceerrorMessage})
    }
    discounterrorMessage=validate_discount(book_discount)
    if(discounterrorMessage!=="Success"){
        setdiscountErrorMessage({discounterrorMessage:discounterrorMessage})
    }
    availabilityerrorMessage=validate_stock(book_availability)
    if(availabilityerrorMessage!=="Success"){
        setavailabilityErrorMessage({availabilityerrorMessage:availabilityerrorMessage})
    }
    authorerrorMessage=validate_discount(book_author)
    if(authorerrorMessage!=="Success"){
        setauthorErrorMessage({authorerrorMessage:authorerrorMessage})
    }

    publishererrorMessage=validate_publisher_name(book_publisher)
    if(publishererrorMessage!=="Success"){
        setpublisherErrorMessage({publishererrorMessage:publishererrorMessage})
    }


    const [deserrorMessage, setdesErrorMessage] = useState("");
  const [priceerrorMessage, setpriceErrorMessage] = useState("");
  const [discounterrorMessage, setdiscountErrorMessage] = useState("");
  const [availabilityerrorMessage, setavailabilityErrorMessage] =useState("");
  const [authorerrorMessage, setauthorErrorMessage] = useState("");
  const [publishererrorMessage, setpublisherErrorMessage] = useState("");
  const [typeerrorMessage, settypeErrorMessage] = useState("");
  const [bmessage, setBMessage] = useState("");
  const [dmessage, setDMessage] = useState("");
  const [pmessage, setPMessage] = useState("");
  const [dsmessage, setDSMessage] = useState("");
  const [amessage, setAMessage] = useState("");
  const [aamessage, setAAMessage] = useState("");
  const [ppmessage, setPPMessage] = useState("");
  const [tmessage, setTMessage] = useState("");