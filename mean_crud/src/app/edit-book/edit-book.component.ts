import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { FormControl,FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BookService } from '../service/book.service';
import { Book } from '../model/book.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  
  books: Book = new Book();
  id:any;
  data:any;
  constructor(private book:BookService,private route:ActivatedRoute,private toastr:ToastrService,private router:Router) { }

  form=new FormGroup({
    bookname:new FormControl(''),
    book_description:new FormControl(''),
    book_price:new FormControl(''),
    book_discount:new FormControl(''),
    book_availability:new FormControl(''),
    book_author:new FormControl(''),
    book_publisher:new FormControl('')
  })

  ngOnInit(): void {
    console.log("hai")
    // this.id=this.route.snapshot.params[this.id];
    this.id=this.route.snapshot.params['id'];
   
    console.log(this.id)
    this.getData();
  }

  getData(){
    this.book.getDataById(this.id).subscribe(res=>{
      this.data=res;
      this.books=this.data;
      console.log(this.books)
      this.form=new FormGroup({
    bookname:new FormControl(this.books.bookname),
    book_description:new FormControl(this.books.book_description),
    book_price:new FormControl(this.books.book_price),
    book_discount:new FormControl(this.books.book_discount),
    book_availability:new FormControl(this.books.book_availability),
    book_author:new FormControl(this.books.book_author),
    book_publisher:new FormControl(this.books.book_publisher)

      })
    })
  }

  updateData(){
    this.book.updateData(this.id,this.form.value).subscribe(res=>{
      this.data=res;
      console.log(this.data)
      this.toastr.success("data updated successfully")
      this.router.navigateByUrl("/")
    })
  }
}
