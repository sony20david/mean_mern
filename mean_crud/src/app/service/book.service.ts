import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BookService {


  constructor(private httpClient:HttpClient) { }

  getData(){
    return this.httpClient.get('http://localhost:4002/users/')
  }

  insertData(data:any){
    return this.httpClient.post('http://localhost:4002/users/add',data)
  }

  getDataById(id:any){
    return this.httpClient.get('http://localhost:4002/users/'+id)
  }

  updateData(id:any,data:any){
    return this.httpClient.put('http://localhost:4002/users/'+id,data)
  }
}
