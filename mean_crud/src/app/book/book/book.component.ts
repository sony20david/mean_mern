import { Component, OnInit } from '@angular/core';

import { BookService } from 'src/app/service/book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  books:any;
  constructor(private bookService:BookService) { }

  ngOnInit(): void {
    this.getBooksData()
  }

  getBooksData(){
    this.bookService.getData().subscribe(res=>{
      console.log(res)
      this.books=res;
    })
  }

}
