import { Component, OnInit } from '@angular/core';

import { BookService } from 'src/app/service/book.service';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';




@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  form!:FormGroup;
  // submitted!:false;
  submitted:boolean=false;
  data:any;
  constructor(private bookService:BookService,private formBuilder:FormBuilder,private router:Router,private tostr:ToastrService) {
    
   }


   createForm(){
     this.form=this.formBuilder.group({
       bookname:['',Validators.required],
       book_description:['',Validators.required],
       book_price:['',Validators.required],
       book_discount:['',Validators.required],
       book_availability:['',Validators.required],
       book_author:['',Validators.required],
       book_publisher:['',Validators.required]
     })
   }

  ngOnInit(): void {
    this.createForm();
  }

  get f(){
    return this.form.controls;
   }
  insertData(){
    this.submitted=true;
    // alert(this.form.value.bookname)

    if(this.form.invalid){
      return;
    }

    this.bookService.insertData(this.form.value).subscribe(res=>{
      this.data=res;
      this.tostr.success("data saved successfully")
      this.router.navigateByUrl("/")
    })
  }

}
