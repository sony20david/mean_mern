import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule,Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './book/book/book.component';
import { NavbarComponent } from './navbar/navbar/navbar.component';
import { AddBookComponent } from './book/add-book/add-book.component';

import {HttpClientModule} from '@angular/common/http';


import { ToastrModule } from 'ngx-toastr';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { EditBookComponent } from './edit-book/edit-book.component';

const appRoutes:Routes=[{
  path:'',component:BookComponent
},
{
  path:'add-book',component:AddBookComponent
},
{
  path:'edit/:id',component:EditBookComponent
}
]

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    NavbarComponent,
    AddBookComponent,
    EditBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
