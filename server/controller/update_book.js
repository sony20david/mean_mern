import { request } from 'express';
import postBook from '../model/book.js';



// export const editBook=async(request,response)=>{
//     let book=await postBook.findById(request.params._id)
//     book=request.body;
//     const editBook=new postBook(book)
//     try {
//         await postBook.updateOne({_id:request.params._id},editBook);
//         response.status(201).json(editBook)
//     } catch (error) {
//         response.status(409).json({message:error.message})       
//     }
// }

export const editBook=(request,response)=>{
const book = {
    bookname:request.body.bookname,
    book_description:request.body.book_description,
    book_price:request.body.book_price,
    book_discount:request.body.book_discount,
    book_availability:request.body.book_availability,
    book_author:request.body.book_author,
    book_publiher:request.body.book_publiher
};
postBook.findByIdAndUpdate(request.params.id, { $set: book }, { new: true }, (err, data) => {
    if(!err) {
        response.status(200).json(data)
    } else {
        console.log(err);
    }
});

}