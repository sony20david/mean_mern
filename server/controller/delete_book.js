import { request } from 'express';
import postBook from '../model/book.js';


export const deleteBook=async(request,response)=>{
    try {
        await postBook.deleteOne({_id:request.params.id});
        response.status(201).json("user deleted")
    } catch (error) {
        response.status(409).json({message:error.message})
    }
}